//alert(`capstone B249!`)

// CAPSTONE PROJECT - SANTILLAN

// CUSTOMER CLASS
	class Customer {
		constructor (name, email) 
		{
			this.name = name;
			this.email = email;
			this.cart = new Cart (name);
			this.orders = [];
		}

		/* CHECKOUT METHOD
			If the Cart is not empty, pushes an object to the
			orders array with the stucture {products: cart contents, totalAmount:
			cart total}
		*/
		checkOut() {
			if (this.cart.contents.length > 0)
			{
				this.orders.push(
				{
					products: this.cart.contents,
					totalAmount: this.cart.totalAmount
				})
				this.emptyCart();
				
			}
			else
			{
				console.log("Add some items to your cart first!")
			}
			return this;

		}

		emptyCart() {
			this.cart = new Cart (name)
		}
	}

//  CART CLASS

	class Cart {
		constructor (name)
		{
			this.owner = name;
			this.contents = [];
			this.totalAmount = 0;
		}
	
		/* ADD TO CART METHOD
			accepts a Product instance and a quantity
			number as arguments, pushes an object with structure: {product:instance of Product class, quantity: number} to the contents property
		*/

		addToCart(product, quantity) {
			this.contents.push(
				{	product : product,
					quantity: quantity
				})
			// this.totalAmount = this.contents.quantity

			this.computeTotal()
			return this;

		}

		/* SHOW CART CONTENTS METHOD
			logs the contents property in the
			console
		*/

		showCartContents() {
			return this.contents;
		}

		/* UPDATE PRODUCT QTY
			takes in a string used to find a
			product in the cart by name, and a new quantity. Replaces the quantity
			of the found product in the cart with the new value.
		*/


		updateProductQuantity(prod2Update, newQuantity) {
			this.contents.forEach(i => 
			{
				//console.log(i.product.productName)
				if (i.product.productName = prod2Update)
				{
					i.quantity = newQuantity
				}	
			})

			return this;

		}

		/* CLEAR CART METHOD
			empties the cart contents
		*/

		clearCartContents() {
			this.contents = [];
			return this;
		}

		/* COMPUTE TOTAL
			iterates over every product in the cart,
			multiplying product price with their respective quantities. Sums up all
			results and sets value as totalAmount.	
		*/

		computeTotal() {
			let tempTotal = 0
			this.contents.forEach(i =>
			{
				tempTotal += (i.product.productPrice*i.quantity)
			})
			this.totalAmount = tempTotal
			return this;

		}
	}

// PRODUCT CLASS
	class Product {
		constructor (prodName, price)
		{
			this.productName = prodName;
			this.productPrice = price;
			this.isActive = true;
		}

		/* ARCHIVE METHOD
			will set isActive to false if it is true to begin with
		*/

		archive() {
			this.isActive = false;
			return this;
		}

		/* UPDATE PRICE
			replaces product price with passed in
			numerical value
		*/

		updatePrice(newValue) {
			if (typeof newValue === "number")
			{
				this.productPrice = newValue
			}
			else {console.log (`must input number as product price`)}
			return this;
		}
	}


//  CUSTOMER CREATION:
	const john = new Customer ('John' , 'john@mail.com')
	const jane = new Customer ('Jane' , 'jane@mail.com')

// PRODUCTS
	const soap = new Product (`soap`, 45);
	const toothpaste = new Product (`toothpaste`, 60);
	const shampoo = new Product (`shampoo`, 80);
	const conditioner = new Product (`conditioner` , 95);

/*
	console logs

	john.cart.addToCart(soap,2)
	

	john.cart.updateProductQuantity(soap, 5)
	
	john.cart.showCartContents()

	john.cart.clearCartContents()
*/